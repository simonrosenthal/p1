#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "p1fxns.h"

#define MAXSIZE 512

int Proc_Handle = 0;
pid_t pid_a[MAXSIZE];
int forkInd = 1;

void onusr(int sig){
  if(sig == SIGUSR1){
    Proc_Handle++;
  }
}

int main(int argc, char *argv[]){

  if(argc != 2){
    printf("%s\n", "need to put in name of file");
    return 0;
  }

  int fd = open(argv[1], O_RDONLY); //file descriptor
  if(fd == -1){
    printf("%s\n", "file not found");
    return 0;
  }

  //initialize variables
  int tmpBuffSize = 500;
  char *tmpBuff = malloc(tmpBuffSize);

  int lineLen = p1getline(fd, tmpBuff, tmpBuffSize);
  while(lineLen != 0){

    //sanitize the line to remove any new line characters
    if(tmpBuff[p1strlen(tmpBuff)-1] == '\n')
      tmpBuff[p1strlen(tmpBuff)-1] = '\0';

    //get the command words
    int j =0;
    int i =0;
    int sc=0;
    char *w = malloc(100);
    char *cmd[50];

    //get the first word
    //NOTE a string needs to be length of all chars + 1
    //for null terminator
    sc = p1getword(tmpBuff, i, w);
    while(sc != -1){
      cmd[j] = p1strdup(w);
      i = sc;
      j++;
      sc = p1getword(tmpBuff, i, w);
    }
    cmd[j] = NULL;

    if(signal(SIGUSR1, onusr) == SIG_ERR) {
  		printf("Signal set failed\n");
  		return EXIT_FAILURE;
  	}
    //fork
    pid_t pid = fork();
    switch(pid){
      case -1:
        printf("ERROR\n");
        break;
      case 0:
        while(!Proc_Handle){
          pause();
        }
        if(execvp(&cmd[0][0], cmd) == -1){
            printf("did not find command\n");
        }
        break;
      default:
        pid_a[forkInd] = pid;
        forkInd++;
        break;
    }

    lineLen = p1getline(fd, tmpBuff, tmpBuffSize);
    free(w);
    for(int q= 0; q < j; q++)
      free(cmd[q]);
  }

  for(int i = 1; i < forkInd; i++){
    kill(pid_a[i], SIGUSR1);
  }
  for(int i = 1; i < forkInd; i++){
    kill(pid_a[i], SIGSTOP);
  }
  for(int i = 1; i < forkInd; i++){
      kill(pid_a[i], SIGCONT);
  }
  while(forkInd > 0){
    wait(NULL);
    forkInd--;
    //printf("%d\n", forkInd);
  }
  //printf("finished\n");
  free(tmpBuff);
  close(fd);
  return 0;
}
