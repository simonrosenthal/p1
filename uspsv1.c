#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "p1fxns.h"

int main(int argc, char *argv[]){

  if(argc != 2){
    printf("%s\n", "need to put in name of file");
    return 0;
  }

  int fd = open(argv[1], O_RDONLY); //file descriptor
  if(fd == -1){
    printf("%s\n", "file not found");
    return 0;
  }

  //initialize variables
  int tmpBuffSize = 500;
  char *tmpBuff = malloc(tmpBuffSize);

  int lineLen = p1getline(fd, tmpBuff, tmpBuffSize);
  while(lineLen != 0){

    //sanitize the line to remove any new line characters
    if(tmpBuff[p1strlen(tmpBuff)-1] == '\n')
      tmpBuff[p1strlen(tmpBuff)-1] = '\0';

    //get the command words
    int j =0;
    int i =0;
    int sc=0;
    char *w = malloc(100);
    char *cmd[50];

    //get the first word
    //NOTE a string needs to be length of all chars + 1
    //for null terminator
    sc = p1getword(tmpBuff, i, w);
    //put word in array if it exists, then
    //loop through the rest of the line repeating the process
    while(sc != -1){
      cmd[j] = p1strdup(w);
      i = sc;
      j++;
      sc = p1getword(tmpBuff, i, w);
    }
    cmd[j] = NULL;
    //fork
    pid_t pid = fork();
    if(pid == 0){
      if(execvp(&cmd[0][0], cmd) == -1){
          printf("did not find command\n");
      }
    }
    for(int i = 0; i < 3; i++){
      wait(&pid);
    }
    lineLen = p1getline(fd, tmpBuff, tmpBuffSize);
    free(w);
    for(int q= 0; q < j; q++)
      free(cmd[q]);
  }


  free(tmpBuff);
  close(fd);
  return 0;

}
