#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <math.h>

int main(int argc, char *argv[]){

  //execvp
  //NOTE: execvp replaces current process with the new process
  char* pa[] = {"/usr/bin/echo", "hello", NULL};
  if(execvp(&pa[0][0], pa) == -1){
    printf("did not run command\n");
  }

  //which
  //used to to determine where a program is stored
  char* programToFind = "which";
  //$which command location
  char* wl = "/usr/bin/which";
  char* args[] = {wl, programToFind, NULL};
  if(execvp(&args[0][0], args) == -1){
    printf("did not find command: %s\n", wl[1]);
  }

  //getenv
  //use $env in bash to get all environment variables
  //  this is how I found the USER env var
  char * a = getenv("USER");
  printf("user env var: %s", a);

  int size = (int)pow(2, 3)+1;
  int pid[size];
  char* programToFind = "which";
  char* wl = "/usr/bin/which";
  char* args[] = {wl, programToFind, NULL};


  //fork
  for(int i = 0; i < 3; i++){
    pid[i] = fork();
    if(pid[i] == 0){
      if(execvp(&args[0][0], args) == -1){
        printf("did not find command: %s\n", wl[1]);
      }
    }
  }
  for(int i = 0; i < 3; i++){
    wait(&pid[i]);
  }
  printf("a\n");

}
